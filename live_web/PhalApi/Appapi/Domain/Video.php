<?php

class Domain_Video {


	public function getVideoList($uid,$p) {
        $rs = array();

        $model = new Model_Video();
        $rs = $model->getVideoList($uid,$p);

        return $rs;
    }

	public function getVideo($uid,$videoid) {
        $rs = array();

        $model = new Model_Video();
        $rs = $model->getVideo($uid,$videoid);

        return $rs;
    }


	public function getClassVideo($videoclassid,$uid,$p){
        $rs = array();

        $model = new Model_Video();
        $rs = $model->getClassVideo($videoclassid,$uid,$p);

        return $rs;
    }

}
