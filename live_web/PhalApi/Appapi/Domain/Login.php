<?php

class Domain_Login {

    public function userLogin($country_code,$user_login,$user_pass) {
        $rs = array();

        $model = new Model_Login();
        $rs = $model->userLogin($country_code,$user_login,$user_pass);

        return $rs;
    }

    public function userReg($country_code,$user_login,$user_pass,$source) {
        $rs = array();
        $model = new Model_Login();
        $rs = $model->userReg($country_code,$user_login,$user_pass,$source);

        return $rs;
    }	
	
    public function userFindPass($country_code,$user_login,$user_pass) {
        $rs = array();
        $model = new Model_Login();
        $rs = $model->userFindPass($country_code,$user_login,$user_pass);

        return $rs;
    }	


}
