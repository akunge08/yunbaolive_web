<?php

class Model_Video extends PhalApi_Model_NotORM {

	
	/* 热门视频 */
	public function getVideoList($uid,$p){

        if($p<1){
            $p=1;
        }
		$nums=20;
		$start=($p-1)*$nums;

		$videoids_s='';
		$where="isdel=0 and status=1 and is_ad=0";  //上架且审核通过
		
		$video=DI()->notorm->video
				->select("*")
				->where($where)
				->order("RAND()")
				->limit($start,$nums)
				->fetchAll();

		foreach($video as $k=>$v){
			
			$v=handleVideo($uid,$v);
            
            $video[$k]=$v;

		}


		return $video;
	}
			
	
	/* 分类视频 */
	public function getClassVideo($videoclassid,$uid,$p){
        if($p<1){
            $p=1;
        }
		$nums=21;
		$start=($p-1)*$nums;
		$where="  isdel='0' and status=1  and classid={$videoclassid}";
		
		$video=DI()->notorm->video
				->select("*")
				->where($where)
				->order("addtime desc")
				->limit($start,$nums)
				->fetchAll();

		
		foreach($video as $k=>$v){
			$v=handleVideo($uid,$v);
            
            $video[$k]=$v;
		}			

		return $video;
		
	}


}
